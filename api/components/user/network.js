const express = require('express');
const router = express.Router();
const response = require('../../../network/response');
//Rethinkdb
const r = require('rethinkdb');
const dataBase = process.env.DB_NAME;
const tableName = 'users';

//search database and table
const searchDataBase = r.db(dataBase);
const searchTable = searchDataBase.table(tableName);



/* add user*/
router.post('/', async (req, res) => {
    let user = req.body;

    try {
        const usuario = await searchTable.insert(user).run(req._rbd)
        if (!usuario) {
            response.error(req,res, "There was an error saving the users", 500);
        }
        response.success(req, res, JSON.stringify(usuario), 200);
    } catch (error) {
        response.error(req,res, error, 500);
    }

})

/* get all books */
router.get('/', async (req, res) => {
    if (!searchDataBase) {
        await r.dbCreate(dataBase).run(req._rdb).then(() => {
            console.log(`Database ${dataBase} was created!`);
        }).catch(err => {
            response.error(req, res, err, 500);
        })
    } else if (!searchTable) {
        //create table
        await searchDataBase.tableCreate(tableName).run(req._rdb).then((result) => {
            console.log(`Table ${tableName} was created!`);
            response.success(req, res, result, 200);
        }).catch(err => {
            response.error(req, res, err, 500);
        })
    } else {
        await searchTable
            .orderBy(r.desc("id"))
            .run(req._rdb)
            .then(result => {
                response.success(req, res, result, 200);
            })
            .catch(error => response.error(req, res, error, 500));
    }
});



module.exports = router;