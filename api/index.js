const express = require('express');
const app = express();
const config = require('../config.js');
require( 'dotenv' ).config();
//Routes
const user = require('./components/user/network');

//Connection to Database
const connect = require('../store/lib-connect');
//Rethinkdb
app.use(connect.connect);

//Routes
app.use('/api/users', user);

app.use(connect.close); // close rdb connection

app.listen(config.api.port, () => {
    console.log(`Server is running on port ${config.api.port}`)
})